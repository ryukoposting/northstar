# Package

version       = "0.1.0"
author        = "ryukoposting"
description   = "A layout engine engine"
license       = "Apache-2.0"
srcDir        = "src"


# Dependencies

requires "nim >= 0.19.2"

requires "sdl2"
requires "asynctools >= 0.1.0"

task test, "run tests":
  exec "nim c -r --threads:on tests/test1.nim"
