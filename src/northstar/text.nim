import asyncfutures, asyncdispatch, macros, tables

# import asynctools/asyncsync

import northstar
import northstar/private/[utils, core, request]
import northstar/private/primitives/[color, dimension]

type
  Text* = object
    index, uid: int
    win: Window

proc newText*(
    win: Window,
    font: string,
    size: int,
    color: string,
    position = Position(x: 0.px, y: 0.px),
    text = "",
    bold = false,
    italic = false,
    underline = false,
    strikethrough = false,
    justify = haLeft,
    scroll = 0.0,
    box = Size(w: 0.px, h: 0.px)): Future[Text] {.async.} =
  var chan: Channel[Result]
  open chan
  win.chan[].send Request(
    kind: reqNewText,
    rntFont: font,
    rntSize: size,
    rntBox: box,
    rntPos: position,
    rntColor: namedColors[color],
    rntText: text,
    rntBold: bold,
    rntItalic: italic,
    rntUnderline: underline,
    rntJustify: justify,
    rntScroll: scroll,
    rntStrikethrough: strikethrough,
    reslt: addr chan)
  var
    gotData = false
    succ: Result
  
  while not gotData:
    (gotData, succ) = chan.tryRecv()
    yield sleepAsync 1
  
  close chan
  result = Text(index: succ.rntIndex, uid: succ.rntUid, win: win)

proc setColor*(t: Text, ck: string) =
  t.win.chan[].send Request(
    kind: reqSetTextColor,
    rscIndex: t.index,
    rscUid: t.uid,
    rscColor: namedColors[ck])
 
proc setColor*(t: Text, ck: Color) =
  t.win.chan[].send Request(
    kind: reqSetTextColor,
    rscIndex: t.index,
    rscUid: t.uid,
    rscColor: ck)

proc `bold=`*(t: Text, b: bool) =
  t.win.chan[].send Request(
    kind: reqSetTextBold,
    rtbIndex: t.index,
    rtbUid: t.uid,
    rtbBoolVal: b)

proc `italic=`(t: Text, b: bool) =
  t.win.chan[].send Request(
    kind: reqSetTextItalic,
    rtbIndex: t.index,
    rtbUid: t.uid,
    rtbBoolVal: b)

proc `underline=`(t: Text, b: bool) =
  t.win.chan[].send Request(
    kind: reqSetTextUnderline,
    rtbIndex: t.index,
    rtbUid: t.uid,
    rtbBoolVal: b)

proc `strikethrough=`(t: Text, b: bool) =
  t.win.chan[].send Request(
    kind: reqSetTextStrikethrough,
    rtbIndex: t.index,
    rtbUid: t.uid,
    rtbBoolVal: b)

proc `scroll=`*(t: Text, f: float) =
  t.win.chan[].send Request(
    kind: reqSetTextScroll,
    rtsIndex: t.index,
    rtsUid: t.uid,
    rtsFloatVal: f)

proc `text=`(t: Text, s: string) {.async.} =
  discard

proc `font=`(t: Text, s: string) {.async.} =
  discard

proc `size=`(t: Text, i: int) {.async.} =
  discard

macro `color=`*(o: Text, c: untyped): untyped =
  result = newTree(nnkStmtList)
  if c.kind == nnkStrLit and not namedColors.hasKey(c.strVal):
      error "\"" & c.strVal & "\" is not a valid color name."
  else:
    result.add(newCall("setColor", o, c))

proc `bold`(t: Text): Future[bool] {.async.} =
  discard

proc `italic`(t: Text): Future[bool] {.async.} =
  discard

proc `underline`(t: Text): Future[bool] {.async.} =
  discard

proc `strikethrough`(t: Text): Future[bool] {.async.} =
  discard

proc `font`(t: Text): Future[string] {.async.} =
  discard

proc `text`(t: Text): Future[string] {.async.} =
  discard

proc `color`*(t: Text): Future[Color] {.async.} =
  var chan: Channel[Result]
  open chan
  t.win.chan[].send Request(
    kind: reqGetTextColor,
    rgtUid: t.uid, rgtIndex: t.index,
    reslt: addr chan)
  var
    gotData = false
    res: Result
  
  while not gotData:
    (gotData, res) = chan.tryRecv()
    yield sleepAsync 1
  
  if res.kind == resErr:
    raise newException(Exception, res.errMsg)
  else:
    result = res.rgcColor
