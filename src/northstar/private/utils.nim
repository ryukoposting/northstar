import locks

type
  MutexPtr*[T] = object
    it: ptr T
    lock: Lock

template `:=`*(a, b: untyped): untyped =
  let a = b
  a
