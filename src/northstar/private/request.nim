import locks, sequtils, asyncfutures, asyncdispatch, threadpool
import sdl2, sdl2/ttf
import utils

# import asynctools/asyncsync

import primitives/[font, dimension]

type
  RequestKind* = enum
    reqQuit,
    reqLoadFont,
    
    reqNewWindow,
    
    reqNewText,
    reqSetTextBold,
    reqSetTextItalic,
    reqSetTextUnderline,
    reqSetTextStrikethrough,
    reqSetTextText,
    reqSetTextFont,
    reqSetTextSize,
    reqSetTextColor,
    reqSetTextPadding,
    reqSetTextMargin,
    reqSetTextScroll,
    
    reqGetTextColor,
    
    reqTest
  
  ResultKind* = enum
    resOk,
    
    resNewWindow,
    
    resNewText,
    resSetTextBold,
    resSetTextItalic,
    resSetTextUnderline,
    resSetTextStrikethrough,
    resSetTextText,
    resSetTextFont,
    resSetTextSize,
    resSetTextColor,
    
    resGetColor
    
    resErr
  
  Request* = object
    case kind*: RequestKind
    of reqNewWindow:
      rnwTitle*: string
      rnwWidth*, rnwHeight*: int
    of reqLoadFont:
      fontName*: string
      defaultFont*: (FontKind, string)
      fonts*: seq[(FontKind, string)]
    of reqNewText:
      rntFont*, rntText*: string
      rntSize*: int
      rntPos*: Position
      rntBox*: Size
      rntColor*: Color
      rntScroll*: float
      rntBold*, rntItalic*, rntUnderline*, rntStrikethrough*: bool
      rntJustify*: HorizontalAlignment
    of reqSetTextColor:
      rscIndex*, rscUid*: int
      rscColor*: Color
    of reqSetTextBold, reqSetTextItalic, reqSetTextUnderline,
       reqSetTextStrikethrough:
      rtbIndex*, rtbUid*: int
      rtbBoolVal*: bool
    of reqSetTextScroll:
      rtsIndex*, rtsUid*: int
      rtsFloatVal*: float
    of reqGetTextColor:
      rgtIndex*, rgtUid*: int
    else:
      discard
    reslt*: ptr Channel[Result]
  
  Result* = ref object
    case kind*: ResultKind
    of resNewWindow:
      rnwWinChan*: ptr Channel[Request]
    of resErr:
      errMsg*: string
    of resNewText:
      rntIndex*: int
      rntUid*: int
    of resGetColor:
      rgcColor*: Color
    else:
      discard

  Win* = ref object
    chan*: Channel[Request]
    renderThread*: Thread[WinInit]
  
  WinInit* = object
    chan*: ptr Channel[Request]
    title*: string
    width*, height*: int
    fontCache*: ptr FontCache
