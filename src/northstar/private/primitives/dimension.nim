type
  DimensionKind* = enum
    dkAbsolute,
    dkPercent,
    dkAuto
  
  Dimension* = object
    case kind*: DimensionKind
    of dkAbsolute:
      px*: int
    of dkPercent:
      pct*: float
    else:
      discard
  
  Position* = object
    x*, y*: Dimension
  
  Size* = object
    w*, h*: Dimension
  
  Padding* = object
    left*, right*, top*, bottom*: Dimension
  
  Margin* = object
    left*, right*, top*, bottom*: Dimension
  
  HorizontalAlignment* = enum
    haLeft,
    haCenter,
    haRight

func px*(i: int): Dimension =
  Dimension(
    kind: dkAbsolute,
    px: i)

func auto*: Dimension =
  Dimension(
    kind: dkAuto)

func percent*(p: float): Dimension =
  Dimension(
    kind: dkPercent,
    pct: p)
