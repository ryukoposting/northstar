# low-level font caching subsystem

import tables, os
import sdl2, sdl2/ttf
import northstar/private/utils

const
  cachedPtrsPerFont = 16

type
  FontVariant = object
    ptrs: OrderedTableRef[int, FontPtr]
    path: string
    hasPath: bool
  
  FontKind* = enum
    fkRegular,
    fkBold,
    fkItalic,
    fkUnderline,
    fkBoldItalic,
    fkBoldUnderline,
    fkItalicUnderline,
    fkBoldItalicUnderline
    
  CacheLine = object
    defaultKind: FontKind
    variant: array[FontKind, FontVariant]
  
  FontCache* = object
    lines: Table[string, CacheLine]

proc initFontCache*: FontCache =
  FontCache(
    lines: initTable[string, CacheLine]())

proc initFontVariant(path: string): FontVariant =
  FontVariant(
    path: path,
    hasPath: true,
    ptrs: newOrderedTable[int, FontPtr](initialSize = cachedPtrsPerFont))

proc initFontVariant: FontVariant =
  FontVariant(
    hasPath: false,
    ptrs: newOrderedTable[int, FontPtr](initialSize = cachedPtrsPerFont))

proc add*(
    fc: var FontCache,
    name: string,
    default: (FontKind, string),
    variants: varargs[(FontKind, string)]) =
  for k in fc.lines.keys:
    assert k != name
  
  fc.lines[name] = CacheLine(
    defaultKind: default[0])
  
  assert fileExists(default[1])
  
  fc.lines[name].variant[default[0]] = initFontVariant(default[1])
  
  for v in variants:
    assert fileExists(v[1])
#     echo "adding ", name, " of kind ", $v[0], " with path ", v[1]
    fc.lines[name].variant[v[0]] = initFontVariant(v[1])
  
  for it in fc.lines[name].variant.mitems:
    if not it.hasPath:
      it = initFontVariant()

proc regular*(path: string): (FontKind, string) = (fkRegular, path)
proc bold*(path: string): (FontKind, string) = (fkBold, path)
proc italic*(path: string): (FontKind, string) = (fkItalic, path)
proc boldItalic*(path: string): (FontKind, string) = (fkBoldItalic, path)

func bits(fk: FontKind): int =
  case fk:
  of fkRegular:
    TTF_STYLE_NORMAL
  of fkBold:
    TTF_STYLE_BOLD
  of fkItalic:
    TTF_STYLE_ITALIC
  of fkUnderline:
    TTF_STYLE_UNDERLINE
  of fkBoldItalic:
    TTF_STYLE_BOLD or TTF_STYLE_ITALIC
  of fkBoldUnderline:
    TTF_STYLE_BOLD or TTF_STYLE_UNDERLINE
  of fkItalicUnderline:
    TTF_STYLE_ITALIC or TTF_STYLE_UNDERLINE
  of fkBoldItalicUnderline:
    TTF_STYLE_BOLD or TTF_STYLE_ITALIC or TTF_STYLE_UNDERLINE

proc get*(fc: var FontCache, fontName: string, size: int, style = fkRegular): FontPtr =
  assert size >= 0
  var
    fl = fc.lines[fontName]
    f = fl.variant[style]
  
  if f.ptrs.hasKey(size): # cache hit
    result = f.ptrs[size]
    # move the hit member to the top of the cache
    f.ptrs.del size
    f.ptrs[size] = result
  elif f.ptrs.len < cachedPtrsPerFont:  # cache miss with open spot in cache
    result =
      if f.hasPath:
        openFont(f.path, size.cint)
      else:
        openFont(fl.variant[fl.defaultKind].path, size.cint)
    if not f.hasPath:
      # need to manually apply the font style since it wasn't
      # loaded from a TTF that already has that style
      result.setFontStyle style.bits().cint
    f.ptrs[size] = result
  else:  # miss with full cache
    # HACK to get the first key in the table
    var kick = -1
    for k in f.ptrs.keys:
      kick = k
      break
    
    # destroy the least-recently-used cached font
    f.ptrs[kick].close()
    f.ptrs.del kick
    
    result =
      if f.hasPath:
        openFont(f.path, size.cint)
      else:
        openFont(fl.variant[fl.defaultKind].path, size.cint)
    if not f.hasPath:
      # need to manually apply the font style since it wasn't
      # loaded from a TTF that already has that style
      result.setFontStyle style.bits().cint
    f.ptrs[size] = result
