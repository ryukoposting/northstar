import tables
import sdl2

export sdl2.Color

const
  namedColors* = {
    "White": color(255, 255, 255, 255),
    "Black": color(0, 0, 0, 255),
    "Red": color(255, 0, 0, 255),
    "Green": color(0, 255, 0, 255),
    "Blue": color(0, 0, 255, 255),
    "Yellow": color(255, 255, 0, 255),
    "Cyan": color(0, 255, 255, 255),
    "Fuchsia": color(255, 0, 255, 255),
    "Gray": color(128, 128, 128, 255),
    "Grey": color(128, 128, 128, 255),
    "ForestGreen": color(34, 139, 34, 255),
    "None": color(0, 0, 0, 0)
  }.toTable
