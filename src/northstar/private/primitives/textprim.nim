import sdl2, sdl2/ttf
import northstar/private/primitives/[font, color, dimension]
import sequtils, strutils, math

type
  TextPrim* = ref object
    font: string
    style: FontKind
    pos*: Position
    box*: Size
    justify: HorizontalAlignment
    size: int
    text: string
    color: Color
    surface: SurfacePtr
    rerender: bool
    texture: TexturePtr
    scrol: float

proc `bottom`*(t: TextPrim): int = t.pos.y.px + t.box.h.px

proc `top`*(t: TextPrim): int = t.pos.y.px

proc `left`*(t: TextPrim): int = t.pos.x.px

proc `right`*(t: TextPrim): int = t.pos.x.px + t.box.w.px

proc `scroll`*(t: TextPrim): float = t.scrol

proc `color`*(t: TextPrim): Color = t.color

proc `scroll=`*(t: var TextPrim, s: float) =
  if s > 1.0:
    t.scrol = 1.0
  elif s < 0.0:
    t.scrol = 0.0
  else:
    t.scrol = s

proc `color=`*(t: var TextPrim, c: Color) =
  t.color = c
  t.rerender = true

proc spaceWidth(f: FontPtr): cint =
  assert f.glyphMetrics(' '.uint16, nil, nil, nil, nil, addr result) == 0

proc stringWidth(f: FontPtr, s: string): cint =
  assert f.sizeUtf8(s, addr result, nil) == 0

proc makeValidLine(
    words: var seq[tuple[str: string, width: cint]],
    font: FontPtr,
    spWidth: cint,
    maxWidth: int): tuple[str: string, width: cint] =
  if words.len > 0:
    if words[0].width > maxWidth:
      let w = words[0]
      words.delete(0, 0)
      for i in countdown(w.str.len - 1, 1):
        result.str = w.str[0..i - 1]
        result.width = font.stringWidth(result.str)
        if result.width <= maxWidth:
          let s = w.str[i..^1]
          words.insert(@[(str: s, width: font.stringWidth(s))], 0)
          break
    else:
      result.str = words[0].str
      result.width = words[0].width
      words.delete(0, 0)
      var endi = -1
      for i, w in words:
        if result.width + spWidth + w.width <= maxWidth:
          result.width += spWidth + w.width
          result.str.add " "
          result.str.add w.str
          endi = i
        else:
          break
      if endi >= 0:
        words.delete(0, endi)
        

proc createValidLines(
    t: TextPrim,
    font: FontPtr): seq[tuple[str: string, width: cint]] =
  result = @[]
  let
    spWidth = font.spaceWidth()
    paragraphs = t.text.splitlines().mapIt(it.split())
  if t.text.len > 0:
    for strs in paragraphs:
      var words = strs.mapIt((str: it, width: stringWidth(font, it)))
      while words.len > 0:
        result.add makeValidLine(words, font, spWidth, t.box.w.px)
    
proc createTextureLeft(
    t: TextPrim,
    renderer: var RendererPtr,
    fc: var FontCache): TexturePtr =
  result = t.texture
  if t.rerender:
    if t.surface != nil:
      t.surface.freeSurface()
    t.surface = fc.get(t.font, t.size, t.style).renderUtf8BlendedWrapped(
      t.text, t.color, cast[uint32](t.box.w.px))
    
    if t.texture != nil:
      destroyTexture t.texture
    result = renderer.createTextureFromSurface(t.surface)
    t.rerender = false


proc createTextureRight(
    t: TextPrim,
    renderer: var RendererPtr,
    fc: var FontCache): TexturePtr =
  result = t.texture
  if t.rerender:
    if t.surface != nil:
      t.surface.freeSurface()
    var font = fc.get(t.font, t.size, t.style)
    let
      lineSkip = font.fontLineSkip()
      lines = createValidLines(t, font)
    t.surface = createRgbSurface(0,
      t.box.w.px.cint,
      lineSkip.cint * lines.len.cint,
      32, 0, 0, 0, 0)
    for i, line in lines:
      var
        lineSurf = fc.get(t.font, t.size, t.style).renderUtf8BlendedWrapped(
          line.str, t.color, cast[uint32](t.box.w.px))
        destRect = rect(
          t.box.w.px.cint - line.width.cint, i.cint * lineSkip.cint,
          line.width.cint, lineSkip.cint)
        srcRect = rect(
          0.cint, 0.cint, line.width.cint, lineSkip.cint)
        
      assert lineSurf.blitSurface(addr srcRect, t.surface, addr destRect) == SdlSuccess
      lineSurf.freeSurface()
    
    if t.texture != nil:
      destroyTexture t.texture
    result = renderer.createTextureFromSurface(t.surface)
    t.rerender = false
  
  

proc drawTextPrim(
    t: TextPrim,
    renderer: var RendererPtr,
    fc: var FontCache): TexturePtr =
  case t.justify:
  of haLeft:
    result = createTextureLeft(t, renderer, fc)
  of haRight:
    result = createTextureRight(t, renderer, fc)
  of haCenter:
    discard
  var
    clipRect = rect(
      t.pos.x.px.cint,
      t.pos.y.px.cint,
      t.box.w.px.cint,
      t.box.h.px.cint)
    top = if t.surface.h < t.box.h.px: 0 else: ((t.surface.h - t.box.h.px).float * t.scroll).int
    h = if t.surface.h < t.box.h.px: t.surface.h else: t.box.h.px.cint
    w = if t.surface.w < t.box.w.px: t.surface.w else: t.box.w.px.cint
    src = rect(0, top.cint, w.cint, h.cint)
    dest = rect(
      t.pos.x.px.cint,
      t.pos.y.px.cint,
      w.cint, h.cint)
    
  if t.box.w.px > 0 or t.box.h.px > 0:
    discard renderer.setClipRect(addr clipRect)
  renderer.copyEx(
    result,
    src,
    dest,
    angle = 0.0,
    center = nil,
    flip = SDL_FLIP_NONE)
  if t.box.w.px > 0 or t.box.h.px > 0:
    discard renderer.setClipRect(nil)

proc newTextPrim*(
    font: string,
    size: int,
    text: string,
    color: Color,
    box: Size,
    pos: Position,
    scroll: float,
    justify: HorizontalAlignment): TextPrim =
  new result
  result.font = font
  result.size = size
  result.text = text
  result.color = color
  result.rerender = true
  result.pos = pos
  result.justify = justify
  result.scroll = scroll
  result.surface = nil

proc `bold=`*(t: var TextPrim, b: bool) =
  case t.style:
  of fkRegular:
    if b:
      t.style = fkBold
      t.rerender = true
  of fkItalic:
    if b:
      t.style = fkBoldItalic
      t.rerender = true
  of fkBold:
    if not b:
      t.style = fkRegular
      t.rerender = true
  of fkUnderline:
    if b:
      t.style = fkBoldUnderline
      t.rerender = true
  of fkBoldItalic:
    if not b:
      t.style = fkItalic
      t.rerender = true
  of fkBoldUnderline:
    if not b:
      t.style = fkUnderline
      t.rerender = true
  of fkItalicUnderline:
    if b:
      t.style = fkBoldItalicUnderline
      t.rerender = true
  of fkBoldItalicUnderline:
    if not b:
      t.style = fkItalicUnderline
      t.rerender = true

proc `italic=`*(t: var TextPrim, b: bool) =
  case t.style:
  of fkRegular:
    if b:
      t.style = fkItalic
      t.rerender = true
  of fkItalic:
    if not b:
      t.style = fkRegular
      t.rerender = true
  of fkBold:
    if b:
      t.style = fkBoldItalic
      t.rerender = true
  of fkUnderline:
    if b:
      t.style = fkItalicUnderline
      t.rerender = true
  of fkBoldItalic:
    if not b:
      t.style = fkBold
      t.rerender = true
  of fkItalicUnderline:
    if not b:
      t.style = fkUnderline
      t.rerender = true
  of fkBoldUnderline:
    if b:
      t.style = fkBoldItalicUnderline
      t.rerender = true
  of fkBoldItalicUnderline:
    if not b:
      t.style = fkBoldUnderline
      t.rerender = true

proc draw*(renderer: var RendererPtr, t: TextPrim, fc: var FontCache) =
  # echo $t.box
  # echo $t.pos
  t.texture = drawTextPrim(t, renderer, fc)
