import locks, sequtils, asyncfutures, asyncdispatch, threadpool, times
import sdl2, sdl2/ttf
import utils

# import asynctools/asyncsync

import primitives/[font, textprim, dimension]
import request

var
  reqChan*: Channel[Request]
  fontCacheLock: Lock


proc renderThread(init: WinInit) {.thread.}
proc masterThread {.thread.}

proc initCore*: void =
  assert sdl2.init(INIT_VIDEO or INIT_TIMER or INIT_EVENTS)
  assert ttfInit() != SdlError
  assert setHint("SDL_RENDER_SCALE_QUALITY", "2")
  initLock fontCacheLock
  open reqChan
  spawn masterThread()


proc newWindow(
    title: string,
    width, height: int,
    fontCache: ptr FontCache): Win =
  new result
  open result.chan
  createThread[WinInit](
    result.renderThread,
    renderThread,
    WinInit(
      chan: addr result.chan,
      title: title,
      width: width,
      height: height,
      fontCache: fontCache))


proc render(
    rp: var RendererPtr,
    text: seq[TextPrim],
    fontCache: var FontCache) =
  rp.clear()
  for t in text:
    rp.draw(t, fontCache)
  rp.present()


proc firstOpenSlot[T](xs: seq[(bool, int, T)]): int =
  result = -1
  for i, x in xs:
    if x[0]:
      result = i
      break


proc masterThread {.thread.} =
  var
    alive = true
    windows: seq[Win] = @[]
    fontCache = initFontCache()
  
  while alive:
    var (gotReq, req) = tryRecv reqChan
    if gotReq:
      case req.kind:
      of reqLoadFont:
        acquire fontCacheLock
        fontCache.add(req.fontName, req.defaultFont, req.fonts)
        release fontCacheLock
      of reqNewWindow:
        windows.add newWindow(
          title = req.rnwTitle,
          width = req.rnwWidth,
          height = req.rnwHeight,
          fontCache = addr fontCache)
        req.reslt[].send Result(
            kind: resNewWindow,
            rnwWinChan: addr windows[windows.high].chan)
      of reqQuit:
        for w in windows:
          w.chan.send Request(kind: reqQuit)
      else:
        echo "ERROR: received requet of type ", req.kind
        assert false


proc renderThread(init: WinInit) {.thread.} =
  var
    wp = createWindow(
      title = init.title,
      x = SDL_WINDOWPOS_CENTERED,
      y = SDL_WINDOWPOS_CENTERED,
      w = init.width.cint,
      h = init.height.cint,
      flags = SDL_WINDOW_SHOWN)
    rp = wp.createRenderer(
      index = -1,
      flags = Renderer_Accelerated or Renderer_PresentVsync)
    text: seq[tuple[open: bool, uid: int, t: TextPrim]] = @[]
    alive = true
    showText = false
    textInput = off
    
    lastDraw = initTime(0, 0)
    refreshPeriod = initDuration(nanoseconds = 10000000)
  
  while alive:
    var (gotRequest, req) = tryRecv(init.chan[])
    if gotRequest:
      case req.kind:
      of reqQuit:
        alive = false
      of reqLoadFont:
        acquire fontCacheLock
        init.fontCache[].add(req.fontName, req.defaultFont, req.fonts)
        release fontCacheLock
      of reqNewText:
        if (i := text.firstOpenSlot()) != -1:
          acquire fontCacheLock
          text[i] = (
            open: false,
            uid: 0, 
            t: newTextPrim(
              font = req.rntFont,
              size = req.rntSize,
              text = req.rntText,
              color = req.rntColor,
              pos = req.rntPos,
              box = req.rntBox,
              scroll = req.rntScroll,
              justify = req.rntJustify))
          text[i].t.bold = req.rntBold
          text[i].t.box = Size(w: req.rntBox.w, h: req.rntBox.h)
          text[i].t.italic = req.rntItalic
          release fontCacheLock
          req.reslt[].send Result(
            kind: resNewText,
            rntIndex: text.high,
            rntUid: text[text.high].uid)
        else:
          acquire fontCacheLock
          text.add (
            open: false,
            uid: 0, 
            t: newTextPrim(
              font = req.rntFont,
              size = req.rntSize,
              text = req.rntText,
              color = req.rntColor,
              pos = req.rntPos,
              box = req.rntBox,
              scroll = req.rntScroll,
              justify = req.rntJustify))
          text[text.high].t.box = Size(w: req.rntBox.w, h: req.rntBox.h)
          text[text.high].t.bold = req.rntBold
          text[text.high].t.italic = req.rntItalic
          release fontCacheLock
          
          req.reslt[].send Result(
            kind: resNewText,
            rntIndex: text.high,
            rntUid: text[text.high].uid)
      of reqTest:
        for t in text.mitems:
          t.t.bold = true
      of reqSetTextBold:
        if req.rtbIndex >= 0 and
           req.rtbIndex <= text.high and
           text[req.rtbIndex].uid == req.rtbUid:
          text[req.rtbIndex].t.bold = req.rtbBoolVal
      of reqSetTextItalic:
        if req.rtbIndex >= 0 and
           req.rtbIndex <= text.high and
           text[req.rtbIndex].uid == req.rtbUid:
          text[req.rtbIndex].t.italic = req.rtbBoolVal
      of reqSetTextScroll:
        if req.rtsIndex >= 0 and
           req.rtsIndex <= text.high and
           text[req.rtsIndex].uid == req.rtsUid:
          text[req.rtsIndex].t.scroll = req.rtsFloatVal
      of reqSetTextColor:
        if req.rscIndex >= 0 and
           req.rscIndex <= text.high and
           text[req.rscIndex].uid == req.rscUid:
          text[req.rscIndex].t.color = req.rscColor
      of reqGetTextColor:
        if req.rgtIndex >= 0 and
           req.rgtIndex <= text.high and
           text[req.rgtIndex].uid == req.rgtUid:
          req.reslt[].send Result(
            kind: resGetColor,
            rgcColor: text[req.rgtIndex].t.color)
        else:
          req.reslt[].send Result(
            kind: resErr,
            errMsg: "Text object does not exist")
      else:
        discard
    
    var event = defaultEvent
    if pollEvent(event):
      case event.kind
      of QuitEvent:
        alive = false
      of TextInput:
        discard
      of TextEditing:
        discard
      of MouseMotion:
        discard
#         echo "mouse motion"
      else:
        discard
    
    if (curTime := getTime()) - lastDraw >= refreshPeriod:
      lastDraw = curTime
#       echo "rendering, "
      rp.render(text.mapIt(it.t), init.fontCache[])
  
  destroy rp
  destroy wp
  ttfQuit()
  sdl2.quit()
