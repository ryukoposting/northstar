import sdl2, sdl2/ttf
import primitives/dimension, font

type
  ElementKind = enum
    ekLabel,
    ekButton,
    ekGrid,
    ekImage
  
  Element* = ref object
    case kind: ElementKind
    else:
      discard
    padding*: Padding
    margin*: Margin
    onClick*: proc()
