import locks, sequtils, asyncfutures, asyncdispatch, sugar
import sdl2, sdl2/ttf

import northstar/private/primitives/[font, dimension]

import northstar/private/[utils, core, request]

export request.Result, request.ResultKind
export font.FontKind, font.regular, font.bold, font.italic, font.boldItalic
export dimension.Dimension, dimension.Position, dimension.Size, 
  dimension.Padding, dimension.px, dimension.HorizontalAlignment

type
  Window* = ref object
    chan*: ptr Channel[Request]

var
  started = false
  startedLock: Lock

initLock(startedLock)

proc initNorthstar* =
  acquire startedLock
  if not started:
    initCore()
    started = true
  release startedLock

proc quitNorthstar* =
  echo "sending quit request"
  reqChan.send Request(kind: reqQuit)

func isOk*(r: Result): bool =
  r.kind == resOk

func isErr*(r: Result): bool =
  r.kind == resErr

proc loadFont*(
    name: string,
    default: (FontKind, string),
    variants: seq[(FontKind, string)]) {.async.} =
  reqChan.send Request(
    kind: reqLoadFont,
    fontName: name,
    defaultFont: default,
    fonts: variants)


proc createWindow*(
    title: string,
    width, height: int): Future[Window] {.async.} =
  var chan: Channel[Result]
  open chan
  reqChan.send Request(
    kind: reqNewWindow,
    rnwTitle: title,
    rnwWidth: width,
    rnwHeight: height,
    reslt: addr chan)
  
  var
    gotData = false
    res: Result
  
  while not gotData:
    (gotData, res) = chan.tryRecv()
    if not gotData:
      yield sleepAsync 0.2
  
  close chan
  
  assert res.kind == resNewWindow
  result = Window(
    chan: res.rnwWinChan)


# proc testMessage* =
#   reqChan.send Request(
#     kind: reqTest)
