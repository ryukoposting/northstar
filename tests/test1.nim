# This is just an example to get you started. You may wish to put all of your
# tests into a single file, or separate them into multiple `test1`, `test2`
# etc. files (better names are recommended, just make sure the name starts with
# the letter 't').
#
# To run these tests, simply execute `nimble test`.

import unittest, sdl2, macros, os
import northstar, northstar/text
import locks
import tables
import asyncdispatch, asyncfutures, sugar

proc mainAsync {.async.} =
  var
    win = waitFor createWindow("hello, world", 1280, 720)
    win2 = waitFor createWindow("goodbye, world", 400, 400)
  
  await loadFont(
    "IBM Plex Sans",
    regular "res/IBMPlexSans-Regular.ttf",
    @[bold "res/IBMPlexSans-Bold.ttf",
    italic "res/IBMPlexSans-Italic.ttf",
    boldItalic "res/IBMPlexSans-BoldItalic.ttf"])
  
  await sleepAsync 1000

  let t = await win.newText(
    font = "IBM Plex Sans",
    size = 18,
    color = "White",
    text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    italic = true,
    justify = haLeft,
    box = Size(w: 120.px, h: 80.px),
    position = Position(x: 100.px, y: 100.px))
  
  let u = await win.newText(
    font = "IBM Plex Sans",
    size = 18,
    color = "Red",
    text = "This text is red!",
    justify = haRight,
    box = Size(w: 100.px, h: 80.px),
    position = Position(x: 400.px, y: 100.px))
  
  for i in 1..20:
    await sleepAsync 200
    t.scroll = i.float * 0.05
  
  t.color = await u.color
  
  await sleepAsync 1000
  
proc main =
  initNorthstar()
  waitFor mainAsync()
  quitNorthstar()

main()


dumpTree:
  body: View {
    backgroundColor: 0xFFFF4F9,
    color: 0x222222
  }
  
  myButton: Button {
    minWidth: 120,
    maxWidth: 180,
    padding: (20, 20, 20, 20),
    margin: (5, 5, 5, 5)
  }
  
  win1: Window {
    width: 1280,
    height: 720,
    gridRows: (0.8 * width, 50.px, 50.px, auto)
  }


# dumpTree:
#  let win = newWindow(
#    "window title",
#    fullScreen = false,
#    hardwareAcceleration = true,
#    width = loadWidth(),
#    height = loadHeight())
